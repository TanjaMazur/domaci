CREATE DATABASE  IF NOT EXISTS `termin7` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `termin7`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: termin7
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drzava` (
  `kod` varchar(3) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  PRIMARY KEY (`kod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES ('100','Srbija'),('200','SAD'),('300','Spanija'),('400','Velika Britanija'),('500','Nemacka');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grad`
--

DROP TABLE IF EXISTS `grad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grad` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `pttbroj` varchar(10) NOT NULL,
  `naziv` varchar(45) NOT NULL,
  `drzavaID` varchar(3) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_grad_1_idx` (`drzavaID`),
  CONSTRAINT `fk_grad_1` FOREIGN KEY (`drzavaID`) REFERENCES `drzava` (`kod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grad`
--

LOCK TABLES `grad` WRITE;
/*!40000 ALTER TABLE `grad` DISABLE KEYS */;
INSERT INTO `grad` VALUES (1,'21000','Novi Sad','100'),(2,'11000','Beograd','100'),(3,'12345','Madrid','300'),(4,'22354','Barselona','300'),(5,'99569','Cikago','200'),(6,'55698','Boston','200'),(7,'99856','London','400'),(8,'15697','Mancester','400'),(9,'56954','Dortmun','500'),(10,'12395','Stutgard','500');
/*!40000 ALTER TABLE `grad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorija`
--

DROP TABLE IF EXISTS `kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorija` (
  `kategorijaID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`kategorijaID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorija`
--

LOCK TABLES `kategorija` WRITE;
/*!40000 ALTER TABLE `kategorija` DISABLE KEYS */;
INSERT INTO `kategorija` VALUES (1,'slatkisi'),(2,'pizza'),(3,'ulje'),(4,'kobasica'),(5,'ostalo');
/*!40000 ALTER TABLE `kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica` (
  `prodavnicaID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `gradID` int(11) NOT NULL,
  PRIMARY KEY (`prodavnicaID`),
  KEY `fk_prodavnica_1_idx` (`gradID`),
  CONSTRAINT `fk_prodavnica_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (1,'Mercator',1),(2,'Big',2),(3,'Lidl',9),(4,'Las Rozas Village',3),(5,'Zara',4),(6,'Corner shop',7),(7,'Tast of Britain',8),(8,'Wolmart',5),(9,'Target',6),(10,'Deutschland',10);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica_proizvod`
--

DROP TABLE IF EXISTS `prodavnica_proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodavnica_proizvod` (
  `proizvodID` int(11) NOT NULL,
  `prodavnicaID` int(11) NOT NULL,
  `cena` varchar(45) DEFAULT NULL,
  KEY `fk_prodavnica_proizvod_1_idx` (`proizvodID`),
  KEY `fk_prodavnica_proizvod_2_idx` (`prodavnicaID`),
  CONSTRAINT `fk_prodavnica_proizvod_1` FOREIGN KEY (`proizvodID`) REFERENCES `proizvod` (`proizvodID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prodavnica_proizvod_2` FOREIGN KEY (`prodavnicaID`) REFERENCES `prodavnica` (`prodavnicaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica_proizvod`
--

LOCK TABLES `prodavnica_proizvod` WRITE;
/*!40000 ALTER TABLE `prodavnica_proizvod` DISABLE KEYS */;
INSERT INTO `prodavnica_proizvod` VALUES (1,10,'563,99'),(2,9,'1199,99'),(3,8,'1499,99'),(4,7,'999,99'),(5,6,'219,99'),(6,5,'115999,99'),(7,4,'16999,99'),(8,3,'499,99'),(9,2,'79,99'),(10,1,'799,99');
/*!40000 ALTER TABLE `prodavnica_proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvod` (
  `proizvodID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `barcod` varchar(13) NOT NULL,
  `proizvodjacID` int(11) NOT NULL,
  `kategorijaID` int(11) NOT NULL,
  PRIMARY KEY (`proizvodID`),
  KEY `fk_proizvod_1_idx` (`proizvodjacID`),
  KEY `fk_proizvod_2_idx` (`kategorijaID`),
  CONSTRAINT `fk_proizvod_1` FOREIGN KEY (`proizvodjacID`) REFERENCES `proizvodjac` (`prozvodID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_2` FOREIGN KEY (`kategorijaID`) REFERENCES `kategorija` (`kategorijaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1,'Biskvit','1234567891234',1,1),(2,'keks','2314567891234',2,1),(3,'Madjarica','3456789123456',3,2),(4,'Maslinovo ulje','4567891234567',4,3),(5,'Ulje','5678912345678',5,3),(6,'mobilni','6789123456789',6,5),(7,'office','7891234567891',7,5),(8,'Bombone','8912345678912',8,1),(9,'Cokoladica','9123456789123',9,1),(10,'Kobasica','5412397851235',10,4);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proizvodjac` (
  `prozvodID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  `gradID` int(11) NOT NULL,
  PRIMARY KEY (`prozvodID`),
  KEY `fk_proizvodjac_1_idx` (`gradID`),
  CONSTRAINT `fk_proizvodjac_1` FOREIGN KEY (`gradID`) REFERENCES `grad` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (1,'Milka',9),(2,'Najlese zelje',2),(3,'Caribic',1),(4,'Capitano',3),(5,'Ybarra',4),(6,'iphone',5),(7,'microsoft',6),(8,'snicers',7),(9,'mars',8),(10,'bratwurst',10);
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-21 14:42:17

1.Zadatak
SELECT *
from proizvod
join proizvodjac on proizvod.proizvodID=proizvodjacID
join grad on proizvodjac.gradID=gradID
join drzava on grad.drzavaID=drzavaID

2.Zadatak
SELECT count(*)
from proizvod
group by proizvodjacID

3.Zadatak
SELECT * FROM prodavnica
right join grad on prodavnica.gradID=gradID
where gradID=3
group by gradID

4.Zadatak
SELECT * FROM prodavnica_proizvod
join prodavnica join proizvod
where prodavnica.prodavnicaID=3
group by prodavnica.prodavnicaID

5.Zadatak
SELECT *
from proizvod
where kategorijaID=3

6.Zadatak
SELECT count(*)
from proizvod
where kategorijaID=3





