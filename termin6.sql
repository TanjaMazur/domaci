CREATE DATABASE  IF NOT EXISTS `domaci_Tatjana` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `domaci_Tatjana`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: domaci_Tatjana
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Kupac`
--

DROP TABLE IF EXISTS `Kupac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kupac` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(40) NOT NULL,
  `prezime` varchar(40) NOT NULL,
  `adresa` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kupac`
--

LOCK TABLES `Kupac` WRITE;
/*!40000 ALTER TABLE `Kupac` DISABLE KEYS */;
INSERT INTO `Kupac` VALUES (1,'Tatjana','Mazur','Novi Sad'),(2,'Petar','Petrovic','Beograd'),(3,'Isidora','Pantic','Nis'),(4,'Vukasin','Ilic','Krusevac'),(5,'Ema','Barcal','Subotica');
/*!40000 ALTER TABLE `Kupac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Narudzbenica`
--

DROP TABLE IF EXISTS `Narudzbenica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Narudzbenica` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Narudzbenica`
--

LOCK TABLES `Narudzbenica` WRITE;
/*!40000 ALTER TABLE `Narudzbenica` DISABLE KEYS */;
INSERT INTO `Narudzbenica` VALUES (1,'2023-01-20'),(2,'2023-01-10'),(3,'2023-01-05'),(4,'2023-01-14'),(5,'2023-01-03');
/*!40000 ALTER TABLE `Narudzbenica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Proizvod`
--

DROP TABLE IF EXISTS `Proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Proizvod` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `barcod` varchar(13) NOT NULL,
  `cena` decimal(10,2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Proizvod`
--

LOCK TABLES `Proizvod` WRITE;
/*!40000 ALTER TABLE `Proizvod` DISABLE KEYS */;
INSERT INTO `Proizvod` VALUES (1,'Najlepse bajke sveta','9788660350017',599.00),(2,'Nezne price za laku noc','4007148076122',699.00),(3,'Vuk u pastirskoj odeci','9788678047190',199.00),(4,'Pinokio','9788678047114',199.00),(5,'Mala sirena','9788678047091',199.00);
/*!40000 ALTER TABLE `Proizvod` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-19 11:15:50


1. Zadatak
SELECT naziv FROM domaci_Tatjana.Proizvod;  --ukoliko se misli na sam naziv proizvoda ukoliko se misli na sve podatke o samom proizvodu umesto naziv ide * kao u sledecem primeru za prikaz kupaca

2. Zadatak
SELECT * FROM domaci_Tatjana.Kupac;  --ukokiko se misli da se vide podaci samih kupaca a ne samo recimo ime i prezime, u tom slucaju umesto * bilo bi: 
SELECT ime, prezime FROM domaci_Tatjana.Kupac;

3. Zadatak
SELECT * FROM domaci_Tatjana.Proizvod WHERE barcod LIKE"9%";
SELECT * FROM domaci_Tatjana.Proizvod WHERE barcod LIKE"4%";

4.Zadatak
SELECT * FROM domaci_Tatjana.Narudzbenica WHERE datum>"2023-01-15";






